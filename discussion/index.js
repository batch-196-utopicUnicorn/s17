//Function
/*
	Syntax:
		function functionName(){
			code  block(statement)
		}

*/
var myName = "Unicorn"
function printName(){
	console.log("My name is " + myName);
}

printName();

//fausd();
//result: err, because it is not yet defined

//function declaration vs expressions



// function expression
	// Anonymouse functino - function without a name

let variableFunction = function() {
	console.log('I am from variableFunction')

}

variableFunction();

let funcExpression = function funcName(){
	console.log("Hello from the other side")
}
funcExpression();

// You can reassing declared function and function expression to new anonymous function

declaredFunction();

function declaredFunction(){
	console.log("Hi I am from declaredFunction()")
}

declaredFunction = function(){
	console.log("updated declaredFunction")
}

declaredFunction()

const constantFunction = function(){
	console.log('Initialized with const');
}

constantFunction();

// constantFunction = function(){
// 	console.log('Cannot be reassigned')
// }

// constantFunction();


/*
	Javascript Variable has 3 trypes of scope;
	1. local/block scope
	2. global scope
	3. function scope
*/

{
	let localVar = "Armando Amorsolo"
}

let globalVar = "My. Worldwide"

console.log(globalVar)
// console.log(localVar); //result: error


	

function showNames(){
	//function scoped variable
	var functionVar = "Joe";
	const functionConst = "Nick";
	let functionLet = "Kevin;"
	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}

showNames();

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);

	//nested function

	function newFunction(){
		let name = "Yor";

		function nestedFunction(){
			let nestedName = "Brando"
			console.log(nestedFunction)
			// console.log(nestedName)
		};
		console.log(nestedFunction())
		nestedFunction()
	};
	newFunction()

// Function and Global Scoped Variable

// Global Scoped Variable
let globalName = "Alan";

function myNewFunction2(){
	let nameInside = "Marco";
	console.log(globalName)
}
myNewFunction2();
// console.log(myNewFunction2());


function showSampleAlert(){
	alert("Hello User");
}

// showSampleAlert()
console.log("I will only log in the consile when the alert is dismissed.")

//prompt()
/*
	Syntax:
		prompt("<dialog>")
*/

// let sameplePrompt = prompt("Enter your name:");
// console.log("Hello " + sameplePrompt);

// console.log("Halaw " +prompt("Enter your name:"))

// let sampleNullPrompt = prompt("Don't enter anything");
// console.log(sampleNullPrompt)

// function printWelcomeMessage(){
// 	let firstName = prompt("Enter your first name");
// 	let lastName = prompt("Enter your last name")
// 	console.log("Hello, " + firstName + " " + lastName + "!")
// 	console.log("Welcome to my life")
// }

// printWelcomeMessage();

// Function Naming Conventions

function getCourses(){
	let courses = ["Science 101", "Arithmetic 101", "Geology 102"];
	console.log(courses);
}

getCourses();

//Avoid generic name to avoid confusion
function get(){
	let name = "Annie";
	console.log(name);
};
get()


//Avoid pointless and inappropriate function names

function foo(){
	console.log(25 % 5);
}
foo();

//Name your functions in small caps. Follow camelcase when naming functions

function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");
}

displayCarInfo()



